def write_bin_file (file_name, byte_vec):
    '''Função responsável pela escrita de arquivo binário
       a partir da lista de inteiros.'''
    with open(file_name, "wb") as bin_file:
        bin_file.write(bytearray(byte_vec))

    print("Escrita de binário completa.")

if __name__ == "__main__":
    byte_vec = [0xA2, 0x00, 0x8A, 0x9D, 0x00, 0x04, 0xA9, 0x01, 0x9D, 0x00, 0xD8, 0xE8, 0xD0, 0xF4, 0x60]
    file_name = "TESTE_LOOP_2.nes"
    write_bin_file(file_name, byte_vec)
