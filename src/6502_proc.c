#include <stdio.h>	
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include "6502_proc.h"

#define RED_ANSI	"\e[0;31m"
#define RESET_ANSI	"\e[1;0m"

/* RE-DECLARAÇÃO DE VARIÁVEIS GLOBAIS */
word_t PC;
byte_t AC;
byte_t X;
byte_t Y;
byte_t SR;
byte_t SP;
byte_t PROC_MEM[MEM_MAP_SZ];
word_t ARG_DEF;
byte_t *ARG = (byte_t *) &ARG_DEF;
byte_t STACK_FLAG = 0;

/*  FUNÇÕES DE MANEJO DE SISTEMA */
void print_error (char *msg) {
	/* 
	 * 		RESUMO:
	 *
	 * Função de impressão de erro na saída STDERR. 
	 *
	 */

	fprintf(
		stderr, 
		RED_ANSI
			"ERRO:"
		RESET_ANSI
		" %s\n",
		msg
	);
}

void WRITE_MEM (word_t addr, byte_t data) {
	/* 
	 * 		RESUMO:
	 *
	 * Função de escrita de byte na memória 
	 *
	 */

	if (addr >= 0 && addr <= 0x1FFF)
		PROC_MEM[addr % RAM_MIRROR] = data;
	else
		PROC_MEM[addr] = data;
}

byte_t READ_MEM (word_t addr) {
	/* 
	 * 		RESUMO:
	 *
	 * Função de leitura de byte da memória 
	 *
	 */

	if (addr >= 0 && addr <= 0x1FFF)
		return PROC_MEM[addr % RAM_MIRROR];

	return PROC_MEM[addr];
}

/* CRIAÇÃO DE FUNÇÕES PARA SERVIÇO DE INTERRUPÇÕES */

void IRQ_SERVICE (void) {
	/* 
	 * 		RESUMO:
	 * 
	 * Função de serviço para interrupções
	 * mascaráveis de deiverentes naturezas.
	 * É executada ao fim do pipeline e a-
	 * tende os diferentes registradores de
	 * controle e estado.
	 *
	 * https://www.nesdev.org/wiki/IRQ
	 */

	// Encerra a função
	return;
}

void NMI_SERVICE (void) {
	/* 
	 * 		RESUMO:
	 * 
	 * Função de serviço para interrupções
	 * não mascaráveis (tipicamente de fim
	 * de frame na PPU). Mais informações
	 * em:
	 *
	 * https://www.nesdev.org/wiki/NMI
	 */

	// Encerra a função
	return;
}

/* CRIAÇÃO DE FUNÇÕES DE MANEJO DE MODOS DE ENDEREÇAMENTO */
void ACC_MO (opcode_t *op) {
	/*
	 * 		RESUMO:
	 *
	 * Função de manejo de modo de endereçamento
	 * AC -> Faz argumento apontar p/ AC.
	 * 
	 */

	ARG = &AC;
	strcpy(op->args, "A");
}

void ABS_MO (opcode_t *op) {
	/*
	 * 		RESUMO:
	 *
	 * Função de manejo de modo de endereçamento
	 * ABS -> ARG é endereço absoluto.
	 *
	 */

	ARG = PROC_MEM 
	    + (word_t) ((READ_MEM(PC+2) << 8) 
	    | READ_MEM(PC+1));
	sprintf(
		op->args, 
		"$%04X", 
		(READ_MEM(PC+2) << 8) | READ_MEM(PC+1)
	);
}

void ABX_MO (opcode_t *op) {
	/*
	 * 		RESUMO:
	 *
	 * Função de manejo de modo de endereçamento
	 * ABX -> ARG é endereço absoluto + reg. X
	 *
	 */

	ARG = PROC_MEM 
	    + (word_t) ((READ_MEM(PC+2) << 8) 
	    | READ_MEM(PC+1));
	ARG += X + (SR & CAR_FLAG) ? 1 : 0;
	sprintf(
		op->args, 
		"$%04X, X", 
		(READ_MEM(PC+2) << 8) | READ_MEM(PC+1)
	);
}

void ABY_MO (opcode_t *op) {
	/*
	 * 		RESUMO:
	 *
	 * Função de manejo de modo de endereçamento
	 * ABY -> ARG é endereço absoluto + reg. Y + C
	 *
	 */

	ARG = PROC_MEM 
	    + (word_t) ((READ_MEM(PC+2) << 8) 
	    | READ_MEM(PC+1));
	ARG += Y + (SR & CAR_FLAG) ? 1 : 0;
	sprintf(
		op->args, 
		"$%04X, Y", 
		(READ_MEM(PC+2) << 8) | READ_MEM(PC+1)
	);
}

void IMD_MO (opcode_t *op) {
	/*
	 * 		RESUMO:
	 *
	 * Função de manejo de modo de endereçamento
	 * IMD -> ARG = MEM[PC+1]
	 *
	 */

	ARG = PROC_MEM + PC + 1;
	sprintf(op->args, "#%02X", *ARG);
}

void IMP_MO (opcode_t *op) {
	/*
	 * 		RESUMO:
	 *
	 * Função de manejo de modo de endereçamento
	 * IMP -> Instrução é o próprio operando
	 *
	 */

	ARG_DEF = 0;
	ARG = (byte_t *) &ARG_DEF;
	strcpy(op->args, "\0");
}

void IND_MO (opcode_t *op) {
	/*
	 * 		RESUMO:
	 *
	 * Função de manejo de modo de endereçamento
	 * IND -> &ARG = &mem[mem[PC+2 | PC+1]]
	 *
	 */

	ARG = PROC_MEM 
	    + READ_MEM((READ_MEM(PC+2) << 8) 
	    | READ_MEM(PC+1));
	sprintf(op->args, "($%04X)", *ARG);
}

void XIN_MO (opcode_t *op) {
	/*
	 * 		RESUMO:
	 *
	 * Função de manejo de modo de endereçamento
	 * XIN -> &ARG = &mem[mem[PC+2+X | PC+1+X]]
	 *
	 */

	ARG = PROC_MEM 
	    + READ_MEM(((READ_MEM(PC+2) + X) << 8) 
	    | (READ_MEM(PC+1) + X + 1));
	sprintf(op->args, "($%02X, X)", *ARG);
}

void YIN_MO (opcode_t *op) {
	/*
	 * 		RESUMO:
	 *
	 * Função de manejo de modo de endereçamento
	 * YIN -> &ARG = &mem[mem[PC+2+X | PC+1+X]]
	 *
	 */

	ARG = PROC_MEM 
	    + READ_MEM((READ_MEM(PC+2) << 8) 
	    | READ_MEM(PC+1));
	ARG += Y;
	sprintf(
		op->args, 
		"($%02X), Y", 
		READ_MEM((READ_MEM(PC+2) << 8) | READ_MEM(PC+1))
	);
}

void REL_MO (opcode_t *op) {
	/*
	 * 		RESUMO:
	 *
	 * Função de manejo de modo de endereçamento
	 * REL -> &ARG = PC + (signed) mem[PC+1]
	 *
	 */

	ARG_DEF = PC + (signed char) READ_MEM(PC+1);
	ARG = (byte_t *) &ARG_DEF;
	sprintf(
		op->args, 
		"#%d", 
		(signed char) READ_MEM(PC+1)
	);
}

void ZPG_MO (opcode_t *op) {
	/*
	 * 		RESUMO:
	 *
	 * Função de manejo de modo de endereçamento
	 * ZPG -> &ARG = mem[PC+1]
	 *
	 */

	ARG = PROC_MEM + READ_MEM(PC+1);
	sprintf(op->args, "$%02X", *ARG);
}

void ZPX_MO (opcode_t *op) {
	/*
	 * 		RESUMO:
	 *
	 * Função de manejo de modo de endereçamento
	 * ZPX -> &ARG = mem[PC+1+X]
	 *
	 */

	ARG = PROC_MEM + READ_MEM(READ_MEM(PC+1) + X);
	sprintf(op->args, "$%02X, X", *ARG);
}

void ZPY_MO (opcode_t *op) {
	/*
	 * 		RESUMO:
	 *
	 * Função de manejo de modo de endereçamento
	 * ZPY -> &ARG = mem[PC+1+Y]
	 *
	 */

	ARG = PROC_MEM + READ_MEM(READ_MEM(PC+1) + Y);
	sprintf(op->args, "$%02X, Y", *ARG);
}

/*  FUNÇÕES DE MANEJO DE REGISTRADORES. */
void set_status_reg (byte_t data) {
	/* 
	 * 		RESUMO:
	 *
	 *  Função de ajuste de flags de reg. de status. 
	 *
	 */

	if ((signed char) data < 0) 
		SR |= NEG_FLAG;
	else
		SR &= ~(NEG_FLAG);

	if (data == 0) 
		SR |= ZERO_FLAG;
	else 
		SR &= ~(ZERO_FLAG);

	if (data <= 255 && data >= 0)
		SR &= ~(CAR_FLAG);
	else
		SR |= CAR_FLAG;

	/*  Obs.: O teste para OFV deve ser feito ANTES da soma ou sub. */
	if ((data > 0) && (*ARG > INT_MAX - data))
		SR |= OVF_FLAG;
	else if ((data < 0) && (*ARG < INT_MIN - data))
		SR |= OVF_FLAG;
	else
		SR &= ~(OVF_FLAG);
}

/*  FUNÇÕES DE INSTRUÇÕES EMULADAS. */
void ADC (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Soma de ACC com argumento e Bit de Carry.
	 *
	 */

	byte_t car = (SR & CAR_FLAG) ? 1 : 0;
	AC += *ARG + car;
	set_status_reg(AC);
}

void AND (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Operação de E lógico de ACC com argumento.
	 *
	 */

	AC &= *ARG;
	set_status_reg(AC);
}

void ASL (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Operação de Shift aritmético à esquerda
	 * com bit de carry.
	 *
	 */

	byte_t car = (*ARG & BIT_7) ? 1 : 0;
	AC = *ARG << 1;
	set_status_reg(AC);

	if (car)
		SR |= CAR_FLAG;
	else
		SR &= ~(CAR_FLAG);
}

void BCC (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Operação de salto do PC quando Carry = 0
	 *
	 */

	if ((SR & CAR_FLAG) == 0)
		PC = *ARG | (ARG[1] << 8);
	else
		PC += 2;
}

void BCS (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Operação de salto do PC quando Carry = 1
	 *
	 */

	if (SR & CAR_FLAG)
		PC = *ARG | (ARG[1] << 8);
	else
		PC += 2;
}

void BEQ (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Operação de salto do PC quando Zero = 1
	 *
	 */

	if (SR & ZERO_FLAG)
		PC = *ARG | (ARG[1] << 8);
	else
		PC += 2;
}

void BIT (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Operação de teste de BIT 6 e 6 de argumento
	 * a partir da operação E lógico com reg. AC
	 *
	 */

	byte_t tmp;
	tmp = AC & *ARG;
	set_status_reg(tmp);

	if (tmp & BIT_7)
		SR |= NEG_FLAG;
	else
		SR &= ~(NEG_FLAG);

	if (tmp & BIT_6)
		SR |= OVF_FLAG;
	else
		SR &= ~(OVF_FLAG);
}

void BMI (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Operação de salto do PC quando Neg = 1
	 *
	 */

	if (SR & NEG_FLAG)
		PC = *ARG | (ARG[1] << 8);
	else
		PC += 2;
}

void BNE (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Operação de salto do PC quando Zero = 0
	 *
	 */

	if ((SR & ZERO_FLAG) == 0)
		PC = *ARG | (ARG[1] << 8);
	else
		PC += 2;
}

void BPL (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Operação de salto do PC quando Neg = 0
	 *
	 */

	if ((SR & NEG_FLAG) == 0)
		PC = *ARG | (ARG[1] << 8);
	else
		PC += 2;
}

void BRK (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Operação de habilitação de interrupção IRQ.
	 * (salta para endereço em $0xFFFF - $0xFFFE)
	 *
	 */

	SR |= IRQ_FLAG;
	if ((SP-1 == 0) || (SP-2 == 0) || (SP-3 == 0))
		STACK_FLAG = 1;

	WRITE_MEM(STACK_END + SP--, FORCE_BYTE((PC + 1) >> 8));
	WRITE_MEM(STACK_END + SP--, FORCE_BYTE(PC + 1));
	WRITE_MEM(STACK_END + SP--, SR);
	PC = READ_MEM(0xFFFE) | (READ_MEM(0xFFFF) << 8);
}

void BVC (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Operação de salto do PC quando Overflow = 0
	 *
	 */

	if ((SR & OVF_FLAG) == 0)
		PC = *ARG | (ARG[1] << 8);
	else
		PC += 2;
}

void BVS (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Operação de salto do PC quando Overflow = 1
	 *
	 */

	if (SR & OVF_FLAG)
		PC = *ARG | (ARG[1] << 8);
	else
		PC += 2;
}

void CLC (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Operação de limpeza da flag Carry.
	 *
	 */

	SR &= ~(CAR_FLAG);
}

void CLD (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Operação de limpeza da flag Decimal.
	 *
	 */

	SR &= ~(DEC_FLAG);
}

void CLI (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Operação de limpeza da flag de Interrupção.
	 *
	 */

	SR &= ~(IRQ_FLAG);
}

void CLV (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Operação de limpeza da flag de Overflow.
	 *
	 */

	SR &= ~(OVF_FLAG);
}

void CMP (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Operação de Comparação de reg. AC
	 * com número na memória.
	 *
	 */

	byte_t tmp = AC - *ARG;
	set_status_reg(tmp);
}

void CPX (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Operação de Comparação de reg. X
	 * com número na memória.
	 *
	 */

	byte_t tmp = X - *ARG;
	set_status_reg(tmp);
}

void CPY (void) {
	/* 
	 * 		RESUMO:
	 *
	 * operação de comparação de reg. y
	 * com número na memória.
	 *
	 */

	byte_t tmp = Y - *ARG;
	set_status_reg(tmp);
}

void DEC (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Decrementa número na memória.
	 *
	 */

	(*ARG) --;
	set_status_reg(*ARG);
}

void DEX (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Decrementa reg. X
	 *
	 */

	X--;
	set_status_reg(X);
}

void DEY (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Decrementa reg. Y
	 *
	 */

	Y--;
	set_status_reg(Y);
}

void EOR (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Realiza operação de OU exclusivo
	 * do reg. AC com o argumento
	 *
	 */

	AC ^= *ARG;
	set_status_reg(AC);
}

void INC (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Incrementa número na memória.
	 *
	 */

	(*ARG) ++;
	set_status_reg(*ARG);
}

void INX (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Incrementa reg. X
	 *
	 */

	X++;
	set_status_reg(X);
}

void INY (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Incrementa reg. Y
	 *
	 */

	Y++;
	set_status_reg(Y);
}

void JMP (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Função de salto incondicional.
	 *
	 */

	PC = *ARG | (ARG[1] << 8);
}

void JSR (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Função de salto incondicional p/
	 * subrotina, guardando PC atual na pilha.
	 *
	 */

	if ((SP-1 == 0) || (SP-2 == 0))
		STACK_FLAG = 1;

	WRITE_MEM(STACK_END + SP--, (PC+1) >> 8);
	WRITE_MEM(STACK_END + SP--, FORCE_BYTE(PC+1));
	PC = (word_t) (ARG - (word_t) PROC_MEM);
}

void LDA (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Função de armazenamento de valor 
	 * no reg. AC
	 *
	 */

	AC = *ARG;
	set_status_reg(AC);
}

void LDX (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Função de armazenamento de valor 
	 * no reg. X
	 *
	 */

	X = *ARG;
	set_status_reg(X);
}

void LDY (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Função de armazenamento de 
	 * valor no reg. Y
	 *
	 */

	Y = *ARG;
	set_status_reg(Y);
}

void LSR (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Função de giro aritmético à 
	 * direita com Carry.
	 *
	 */

	byte_t bit_7 = (SR & CAR_FLAG) ? 1 : 0;
	byte_t car   = (*ARG & BIT_0)  ? 1 : 0;
	*ARG >>= 1;
	*ARG |= bit_7;
	set_status_reg(*ARG);
	if (car)
		SR |= CAR_FLAG;
	else
		SR &= ~(CAR_FLAG);
}

void NOP (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Função de instrução que n realiza 
	 * operação.
	 *
	 */

	return;
}

void ORA (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Instrução de OU lógico de ARG com ACC
	 *
	 */

	AC |= *ARG;
	set_status_reg(AC);
}

void PHA (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Instrução de inserção de acumulador 
	 * na pilha.
	 *
	 */

	if (SP-1 == 0)
		STACK_FLAG = 1;

	WRITE_MEM(STACK_END + SP--, AC);
}

void PHP (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Instrução de inserção de reg. 
	 * de status na pilha.
	 *
	 */

	if (SP-1 == 0)
		STACK_FLAG = 1;

	WRITE_MEM(STACK_END + SP--, SR);
}

void PLA (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Instrução de retirada de 
	 * acumulador da pilha.
	 *
	 */

	AC = READ_MEM(STACK_END + SP++);
}

void PLP (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Instrução de retirada de reg 
	 * de status da pilha.
	 *
	 */

	SR = READ_MEM(STACK_END + SP++);
}

void ROL (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Instrução de rotação circular 
	 * à esquerda de byte com C.
	 *
	 */

	byte_t old_c = (SR & CAR_FLAG) ? 1 : 0;
	byte_t new_c = (*ARG & BIT_7)  ? 1 : 0;
	*ARG <<= 1;
	*ARG |= old_c;
	set_status_reg(*ARG);

	if (new_c)
		SR |= CAR_FLAG;
	else
		SR &= ~(CAR_FLAG);
}

void ROR (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Instrução de rotação circular à direita de byte com C.
	 *
	 */

	byte_t old_c = (SR & CAR_FLAG) ? BIT_7 : 0;
	byte_t new_c = (*ARG & BIT_0)  ? BIT_0 : 0;
	*ARG >>= 1;
	*ARG |= old_c;
	set_status_reg(*ARG);

	if (new_c)
		SR |= CAR_FLAG;
	else
		SR &= ~(CAR_FLAG);
}

void RTI (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Instrução de retorno de interrupção.
	 *
	 */

	byte_t LO, HI;
	SR = READ_MEM(STACK_END + SP++);
	LO = READ_MEM(STACK_END + SP++);
	HI = READ_MEM(STACK_END + SP++);
	PC = (HI << 8) | LO;
}

void RTS (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Instrução de retorno de subrotina.
	 *
	 */

	byte_t LO, HI;
	LO = READ_MEM(STACK_END + SP++);
	HI = READ_MEM(STACK_END + SP++);
	PC = (HI << 8) | LO;
}

void SBC (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Instrução de subtração de argumento AC e Carry.
	 *
	 */

	byte_t car = (SR & CAR_FLAG) ? 0 : 1;
	AC -= *ARG - car;
	set_status_reg(AC);
}

void SEC (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Instrução de setagem de bit de Carry
	 *
	 */

	SR |= CAR_FLAG;
}

void SED (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Instrução de setagem da flag decimal.
	 *
	 */

	SR |= DEC_FLAG;
}

void SEI (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Instrução de setagem da flag IRQ.
	 *
	 */

	SR |= IRQ_FLAG;
}

void STA (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Instrução de armazenamento de AC na memória.
	 *
	 */

	*ARG = AC;
}

void STX (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Instrução de armazenamento de X na memória.
	 *
	 */

	*ARG = X;
}

void STY (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Instrução de armazenamento de Y na memória.
	 *
	 */

	*ARG = Y;
}

void TAX (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Instrução de transferência de AC para reg. X
	 *
	 */

	X = AC;
	set_status_reg(X);
}

void TAY (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Instrução de transferência de AC para reg. Y
	 *
	 */

	Y = AC;
	set_status_reg(Y);
}

void TSX (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Instrução de transferência de SP para X
	 *
	 */

	X = SP;
	set_status_reg(X);
}

void TXA (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Instrução de transferência de X para AC
	 *
	 */

	AC = X;
	set_status_reg(AC);
}

void TXS (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Instrução de transferência de X para SP
	 *
	 */

	SP = X;
}

void TYA (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Instrução de transferência de Y para AC
	 *
	 */

	AC = Y;
	set_status_reg(AC);
}

void XXX (void) {
	/* 
	 * 		RESUMO:
	 *
	 * Instrução para opdecodes ilegais.
	 *
	 */

	print_error("Illegal opcode!");
	return;
}
