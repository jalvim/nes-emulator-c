#ifndef __UI_H__
#define __UI_H__

#include <curses.h>

#define KEY_ESC		0x1B
#define MAX_STR		256

/* Índices de cada uma das subjanelas. */
#define DASM_SW		0
#define STAC_SW		1
#define REGS_SW		2

/* Informações de tamanho e posição das subjanelas. */
#define NL_SW		22
#define NC_SW		70
#define BX_DASM		0
#define BY_DASM		0
#define BX_STAC		70
#define BY_STAC		0
#define BX_REGS		0
#define BY_REGS		22

extern int nlines;
extern int ncols;
extern int begx;
extern int begy;
extern WINDOW *main_win;
extern WINDOW *sub_wins[3];

void init_main_win (void);
void destroy_main_win (void);
void print_dasm (char *str);
void print_stac (char *str);
void print_regs (char *str);
int key_input (void);

#endif
