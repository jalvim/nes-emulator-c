#ifndef __6502_H__
#define __6502_H__

/* DECLARAÇÃO DE MACROS DE BITS */
#define BIT_0		1<<0
#define BIT_1		1<<1
#define BIT_2		1<<2
#define BIT_3		1<<3
#define BIT_4		1<<4
#define BIT_5		1<<5
#define BIT_6		1<<6
#define BIT_7		1<<7

#define FORCE_BYTE(X) 	((X) & 0xFF)

/* DECLARAÇÃO DE MACROS DE FLAGS */
#define CAR_FLAG 	1<<0
#define ZERO_FLAG 	1<<1
#define IRQ_FLAG 	1<<2
#define DEC_FLAG 	1<<3
#define BRK_FLAG 	1<<4
#define OVF_FLAG 	1<<6
#define NEG_FLAG 	1<<7

/*  MACROS DE ENDEREÇAMENTO DA PILHA */
#define STACK_BEGIN	0x1FF
#define STACK_END	0X100

/* MACROS DE ENDEREÇO DE VETOR DE INTERRUPÇÃO */
#define INT_ADDR_LO	0XFFFE
#define INT_ADDR_HI	0XFFFF

/* 
 * TODO: Escrever aqui os reg's de ctrl,
 *       serviço e I/O associados às in-
 *       terrupções de cada natureza
 *
 *       Acessar relação em:
 *       https://www.nesdev.org/wiki/IRQ
 * */

/* REGISTRADORES DE CONTROLE E ESTADO DE INTERRUPÇÃO */


/* TAMANHO DA MEMÓRIA VIRTUAL --> MIRRORING */
#define MEM_MAP_SZ	0x10000
#define RAM_MIRROR	0x07FF

/* CRIAÇÃO DE TIPOS ESPECÍFICOS */
typedef unsigned char byte_t;
typedef unsigned short word_t;

typedef struct _opcode_t {
	int cycles;
	int num_bytes;
	char name[4];
	char args[10];
	void (*func) ();
	void (*addr_mode) (struct _opcode_t *);
} opcode_t;

typedef void (*instruct_ptr) (void);

/* CRIAÇÃO DE VARIÁVEIS GLOBAIS */
extern int    cycles;
extern word_t PC;
extern byte_t AC;
extern byte_t X;
extern byte_t Y;
extern byte_t SR;
extern byte_t SP;			// Variável de ponteiro de pilha (sempre na pág 0x0100-0x01FF).
extern byte_t PROC_MEM[MEM_MAP_SZ];
extern byte_t *ARG;			// Variável global de argumento (ajustada em etapa fetch).
extern word_t ARG_DEF;
extern byte_t STACK_FLAG;		// Flag de indicação de stack overflow.

/* DECLARAÇÃO DE FUNÇÕES DE MANEJO DE SISTEMA */
byte_t READ_MEM    (word_t);
void   WRITE_MEM   (word_t, byte_t);
void   fetch_args  (opcode_t *);
void   print_error (char *);

/* DECLARAÇÃO DE FUNÇÕES DE SERVIÇO DE INTERRUPÇÃO */
void IRQ_SERVICE (void);
void NMI_SERVICE (void);

/* DECLARAÇÃO DE FUNÇÕES DE MODO DE ENDEREÇAMENTO */
void ACC_MO (opcode_t *);
void ABS_MO (opcode_t *);
void ABX_MO (opcode_t *);
void ABY_MO (opcode_t *);
void IMD_MO (opcode_t *);
void IMP_MO (opcode_t *);
void IND_MO (opcode_t *);
void XIN_MO (opcode_t *);
void YIN_MO (opcode_t *);
void REL_MO (opcode_t *);
void ZPG_MO (opcode_t *);
void ZPX_MO (opcode_t *);
void ZPY_MO (opcode_t *);

/*  DECLARAÇÃO DE FUNÇÕES DE INSTRUÇÕES */
void ADC (void);
void AND (void);
void ASL (void);
void BCC (void);
void BCS (void);
void BEQ (void);
void BIT (void);
void BMI (void);
void BNE (void);
void BPL (void);
void BRK (void);
void BVC (void);
void BVS (void);
void CLC (void);
void CLD (void);
void CLI (void);
void CLV (void);
void CMP (void);
void CPX (void);
void CPY (void);
void DEC (void);
void DEX (void);
void DEY (void);
void EOR (void);
void INC (void);
void INX (void);
void INY (void);
void JMP (void);
void JSR (void);
void LDA (void);
void LDX (void);
void LDY (void);
void LSR (void);
void NOP (void);
void ORA (void);
void PHA (void);
void PHP (void);
void PLA (void);
void PLP (void);
void ROL (void);
void ROR (void);
void RTI (void);
void RTS (void);
void SBC (void);
void SEC (void);
void SED (void);
void SEI (void);
void STA (void);
void STX (void);
void STY (void);
void TAX (void);
void TAY (void);
void TSX (void);
void TXA (void);
void TXS (void);
void TYA (void);

/* DECLARAÇÃO DE FUNÇÃO P/ INSTRUÇÃO ILEGAL */
void XXX (void);

#endif
