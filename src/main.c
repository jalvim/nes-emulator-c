#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include "6502_proc.h"

/* DEFINIÇÃO DE MACROS PARA ENDEREÇAMENTO NA MEM. VIRTUAL */
#define LOAD_REG	0x4020
#define RESET_VEC_LO	0xFFFC
#define RESET_VEC_HI	0xFFFD

/* RE-DECLARAÇÃO DE VARIÁVEIS GLOBAIS */
int cycles;

/* VARIÁVEL DE CONTROLE DE LOOP PRINCIPAL. */
volatile byte_t stop = 0;

/* CRIAÇÃO DE MARCRO PARA FACILITAÇÃO DA INICIALIZAÇÃO DE MATRIZ DE INST. */
#define INIT_OPCODE(NAME, MODE, LEN, CYCLES) 	{ 			\
	.cycles = CYCLES, 	/* Número de ciclos da instr.      */	\
	.num_bytes = LEN, 	/* Comprimento da instrução.       */	\
	.name = #NAME, 		/* Nome da instrução. 	 --> DEBUG */	\
	.args = "\0", 		/* String de argumentos. --> DEBUG */	\
	.func = NAME,		/* Ponteiro para função da instr.  */	\
	.addr_mode = MODE 	/* Func de Modo de endereçamento.  */	\
}

/*
 *
 * 		MATRIZ DE INSTRUÇÕES:
 *
 * Inicialização da matriz de
 * instrução, escrita como vetor para facilidade
 * de acesso aleatório de elementos.
 *
 */

opcode_t inst_mat[0x100] = {
	[0x00] 		= INIT_OPCODE(BRK, IMP_MO, 0, 7),
	[0x01] 		= INIT_OPCODE(ORA, XIN_MO, 2, 6),
	[0x02 ... 0x04]	= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0x05] 		= INIT_OPCODE(ORA, ZPG_MO, 2, 3),
	[0x06] 		= INIT_OPCODE(ASL, ZPG_MO, 2, 5),
	[0x07]		= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0x08] 		= INIT_OPCODE(PHP, IMP_MO, 1, 3),
	[0x09] 		= INIT_OPCODE(ORA, IMD_MO, 2, 2),
	[0x0A] 		= INIT_OPCODE(ASL, ACC_MO, 1, 2),
	[0x0B ... 0x0C]	= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0x0D] 		= INIT_OPCODE(ORA, ABS_MO, 3, 4),
	[0x0E] 		= INIT_OPCODE(ASL, ABS_MO, 3, 6),
	[0x0F]		= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0x10] 		= INIT_OPCODE(BPL, REL_MO, 2, 2),
	[0x11] 		= INIT_OPCODE(ORA, YIN_MO, 2, 5),
	[0x12 ... 0x14]	= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0x15] 		= INIT_OPCODE(ORA, ZPX_MO, 2, 4),
	[0x16] 		= INIT_OPCODE(ASL, ZPX_MO, 2, 6),
	[0x17]		= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0x18] 		= INIT_OPCODE(CLC, IMP_MO, 1, 2),
	[0x19] 		= INIT_OPCODE(ORA, ABY_MO, 3, 4),
	[0x1A ... 0x1C]	= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0x1D] 		= INIT_OPCODE(ORA, ABX_MO, 3, 4),
	[0x1E] 		= INIT_OPCODE(ASL, ABX_MO, 3, 7),
	[0x1F]		= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0x20] 		= INIT_OPCODE(JSR, ABS_MO, 0, 6),
	[0x21] 		= INIT_OPCODE(AND, XIN_MO, 2, 6),
	[0x22 ... 0x23]	= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0x24] 		= INIT_OPCODE(BIT, ZPG_MO, 2, 3),
	[0x25] 		= INIT_OPCODE(AND, ZPG_MO, 2, 3),
	[0x26] 		= INIT_OPCODE(ROL, ZPG_MO, 2, 5),
	[0x27]		= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0x28] 		= INIT_OPCODE(PLP, IMP_MO, 1, 4),
	[0x29] 		= INIT_OPCODE(AND, IMD_MO, 2, 2),
	[0x2A] 		= INIT_OPCODE(ROL, ACC_MO, 1, 2),
	[0x2B]		= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0x2C] 		= INIT_OPCODE(BIT, ABS_MO, 3, 4),
	[0x2D] 		= INIT_OPCODE(AND, ABS_MO, 3, 4),
	[0x2E] 		= INIT_OPCODE(ROL, ABS_MO, 3, 6),
	[0x2F]		= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0x30] 		= INIT_OPCODE(BMI, REL_MO, 2, 2),
	[0x31] 		= INIT_OPCODE(AND, YIN_MO, 2, 5),
	[0x32 ... 0x34]	= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0x35] 		= INIT_OPCODE(AND, ZPX_MO, 2, 4),
	[0x36] 		= INIT_OPCODE(ROL, ZPX_MO, 2, 6),
	[0x37]		= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0x38] 		= INIT_OPCODE(SEC, IMP_MO, 1, 2),
	[0x39] 		= INIT_OPCODE(AND, ABY_MO, 3, 4),
	[0x3A ... 0x3C]	= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0x3D] 		= INIT_OPCODE(AND, ABX_MO, 3, 4),
	[0x3E] 		= INIT_OPCODE(ROL, ABX_MO, 3, 7),
	[0x3F]		= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0x40] 		= INIT_OPCODE(RTI, IMP_MO, 0, 6),
	[0x41] 		= INIT_OPCODE(EOR, XIN_MO, 2, 6),
	[0x42 ... 0x44]	= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0x45] 		= INIT_OPCODE(EOR, ZPG_MO, 2, 3),
	[0x46] 		= INIT_OPCODE(LSR, ZPG_MO, 2, 5),
	[0x47]		= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0x48] 		= INIT_OPCODE(PHA, IMP_MO, 1, 3),
	[0x49] 		= INIT_OPCODE(EOR, IMD_MO, 2, 2),
	[0x4A] 		= INIT_OPCODE(LSR, ACC_MO, 1, 2),
	[0x4B]		= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0x4C] 		= INIT_OPCODE(JMP, ABS_MO, 0, 3),
	[0x4D] 		= INIT_OPCODE(EOR, ABS_MO, 3, 4),
	[0x4E] 		= INIT_OPCODE(LSR, ABS_MO, 3, 6),
	[0x4F]		= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0x50] 		= INIT_OPCODE(BVC, REL_MO, 2, 2),
	[0x51] 		= INIT_OPCODE(EOR, YIN_MO, 2, 5),
	[0x52 ... 0x54]	= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0x55] 		= INIT_OPCODE(EOR, ZPX_MO, 2, 4),
	[0x56] 		= INIT_OPCODE(LSR, ZPX_MO, 2, 6),
	[0x57]		= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0x58] 		= INIT_OPCODE(CLI, IMP_MO, 1, 2),
	[0x59] 		= INIT_OPCODE(EOR, ABY_MO, 3, 4),
	[0x5A ... 0x5C]	= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0x5D] 		= INIT_OPCODE(EOR, ABX_MO, 3, 4),
	[0x5E] 		= INIT_OPCODE(LSR, ABX_MO, 3, 7),
	[0x5F]		= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0x60] 		= INIT_OPCODE(RTS, IMP_MO, 0, 6),
	[0x61] 		= INIT_OPCODE(ADC, XIN_MO, 2, 6),
	[0x62 ... 0x64]	= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0x65] 		= INIT_OPCODE(ADC, ZPG_MO, 2, 3),
	[0x66] 		= INIT_OPCODE(ROR, ZPG_MO, 2, 5),
	[0x67]		= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0x68] 		= INIT_OPCODE(PLA, IMP_MO, 1, 4),
	[0x69] 		= INIT_OPCODE(ADC, IMD_MO, 2, 2),
	[0x6A] 		= INIT_OPCODE(ROR, ACC_MO, 1, 2),
	[0x6B]		= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0x6C] 		= INIT_OPCODE(JMP, IND_MO, 0, 5),
	[0x6D] 		= INIT_OPCODE(ADC, ABS_MO, 3, 4),
	[0x6E] 		= INIT_OPCODE(ROR, ABS_MO, 3, 6),
	[0x6F]		= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0x70] 		= INIT_OPCODE(BVS, REL_MO, 2, 2),
	[0x71] 		= INIT_OPCODE(ADC, YIN_MO, 2, 5),
	[0x72 ... 0x74]	= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0x75] 		= INIT_OPCODE(ADC, ZPX_MO, 2, 4),
	[0x76] 		= INIT_OPCODE(ROR, ZPX_MO, 2, 6),
	[0x77]		= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0x78] 		= INIT_OPCODE(SEI, IMP_MO, 1, 2),
	[0x79] 		= INIT_OPCODE(ADC, ABY_MO, 3, 4),
	[0x7A ... 0x7C]	= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0x7D] 		= INIT_OPCODE(ADC, ABX_MO, 3, 4),
	[0x7E] 		= INIT_OPCODE(ROR, ABX_MO, 3, 7),
	[0x7F ... 0x80]	= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0x81] 		= INIT_OPCODE(STA, XIN_MO, 2, 6),
	[0x82 ... 0x83]	= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0x84] 		= INIT_OPCODE(STY, ZPG_MO, 2, 3),
	[0x85] 		= INIT_OPCODE(STA, ZPG_MO, 2, 3),
	[0x86] 		= INIT_OPCODE(STX, ZPG_MO, 2, 3),
	[0x87] 		= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0x88] 		= INIT_OPCODE(DEY, IMP_MO, 1, 2),
	[0x8A] 		= INIT_OPCODE(TXA, IMP_MO, 1, 2),
	[0x8B] 		= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0x8C] 		= INIT_OPCODE(STY, ABS_MO, 3, 4),
	[0x8D] 		= INIT_OPCODE(STA, ABS_MO, 3, 4),
	[0x8E] 		= INIT_OPCODE(STX, ABS_MO, 3, 4),
	[0x8F] 		= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0x90] 		= INIT_OPCODE(BCC, REL_MO, 2, 2),
	[0x91] 		= INIT_OPCODE(STA, YIN_MO, 2, 6),
	[0x92 ... 0x93]	= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0x94] 		= INIT_OPCODE(STY, ZPX_MO, 2, 4),
	[0x95] 		= INIT_OPCODE(STA, ZPX_MO, 2, 4),
	[0x96] 		= INIT_OPCODE(STX, ZPY_MO, 2, 4),
	[0x97] 		= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0x98] 		= INIT_OPCODE(TYA, IMP_MO, 1, 2),
	[0x99] 		= INIT_OPCODE(STA, ABY_MO, 3, 5),
	[0x9A] 		= INIT_OPCODE(TXS, IMP_MO, 1, 2),
	[0x9B ... 0x9C]	= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0x9D] 		= INIT_OPCODE(STA, ABX_MO, 3, 5),
	[0x9E ... 0x9F]	= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0xA0] 		= INIT_OPCODE(LDY, IMD_MO, 2, 2),
	[0xA1] 		= INIT_OPCODE(LDA, XIN_MO, 2, 6),
	[0xA2] 		= INIT_OPCODE(LDX, IMD_MO, 2, 2),
	[0xA3] 		= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0xA4] 		= INIT_OPCODE(LDY, ZPG_MO, 2, 3),
	[0xA5] 		= INIT_OPCODE(LDA, ZPG_MO, 2, 3),
	[0xA6] 		= INIT_OPCODE(LDX, ZPG_MO, 2, 3),
	[0xA7] 		= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0xA8] 		= INIT_OPCODE(TAY, IMP_MO, 1, 2),
	[0xA9] 		= INIT_OPCODE(LDA, IMD_MO, 2, 2),
	[0xAA] 		= INIT_OPCODE(TAX, IMP_MO, 1, 2),
	[0xAB] 		= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0xAC] 		= INIT_OPCODE(LDY, ABS_MO, 3, 4),
	[0xAD] 		= INIT_OPCODE(LDA, ABS_MO, 3, 4),
	[0xAE] 		= INIT_OPCODE(LDX, ABS_MO, 3, 4),
	[0xAF] 		= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0xB0] 		= INIT_OPCODE(BCS, REL_MO, 2, 2),
	[0xB1] 		= INIT_OPCODE(LDA, YIN_MO, 2, 5),
	[0xB2 ... 0xB3]	= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0xB4] 		= INIT_OPCODE(LDY, ZPX_MO, 2, 4),
	[0xB5] 		= INIT_OPCODE(LDA, ZPX_MO, 2, 4),
	[0xB6] 		= INIT_OPCODE(LDX, ZPY_MO, 2, 4),
	[0xB7] 		= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0xB8] 		= INIT_OPCODE(CLV, IMP_MO, 1, 2),
	[0xB9] 		= INIT_OPCODE(LDA, ABY_MO, 3, 4),
	[0xBA] 		= INIT_OPCODE(TSX, IMP_MO, 1, 2),
	[0xBB] 		= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0xBC] 		= INIT_OPCODE(LDY, ABX_MO, 3, 4),
	[0xBD] 		= INIT_OPCODE(LDA, ABX_MO, 3, 4),
	[0xBE] 		= INIT_OPCODE(LDX, ABY_MO, 3, 4),
	[0xBF] 		= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0xC0] 		= INIT_OPCODE(CPY, IMD_MO, 2, 2),
	[0xC1] 		= INIT_OPCODE(CMP, XIN_MO, 2, 6),
	[0xC2 ... 0xC3]	= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0xC4] 		= INIT_OPCODE(CPY, ZPG_MO, 2, 3),
	[0xC5] 		= INIT_OPCODE(CMP, ZPG_MO, 2, 3),
	[0xC6] 		= INIT_OPCODE(DEC, ZPG_MO, 2, 5),
	[0xC7] 		= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0xC8] 		= INIT_OPCODE(INY, IMP_MO, 1, 2),
	[0xC9] 		= INIT_OPCODE(CMP, IMD_MO, 2, 2),
	[0xCA] 		= INIT_OPCODE(DEX, IMP_MO, 1, 2),
	[0xCB] 		= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0xCC] 		= INIT_OPCODE(CPY, ABS_MO, 3, 4),
	[0xCD] 		= INIT_OPCODE(CMP, ABS_MO, 3, 4),
	[0xCE] 		= INIT_OPCODE(DEC, ABS_MO, 3, 6),
	[0xCF] 		= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0xD0] 		= INIT_OPCODE(BNE, REL_MO, 2, 2),
	[0xD1] 		= INIT_OPCODE(CMP, YIN_MO, 2, 5),
	[0xD2 ... 0xD4]	= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0xD5] 		= INIT_OPCODE(CMP, ZPX_MO, 2, 4),
	[0xD6] 		= INIT_OPCODE(DEC, ZPX_MO, 2, 6),
	[0xD7] 		= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0xD8] 		= INIT_OPCODE(CLD, IMP_MO, 1, 2),
	[0xD9] 		= INIT_OPCODE(CMP, ABY_MO, 3, 4),
	[0xDA ... 0xDC]	= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0xDD] 		= INIT_OPCODE(CMP, ABX_MO, 3, 4),
	[0xDE] 		= INIT_OPCODE(DEC, ABX_MO, 3, 7),
	[0xDF] 		= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0xE0] 		= INIT_OPCODE(CPX, IMD_MO, 2, 2),
	[0xE1] 		= INIT_OPCODE(SBC, XIN_MO, 2, 6),
	[0xE2 ... 0xE3]	= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0xE4] 		= INIT_OPCODE(CPX, ZPG_MO, 2, 3),
	[0xE5] 		= INIT_OPCODE(SBC, ZPG_MO, 2, 3),
	[0xE6] 		= INIT_OPCODE(INC, ZPG_MO, 2, 5),
	[0xE7] 		= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0xE8] 		= INIT_OPCODE(INX, IMP_MO, 1, 2),
	[0xE9] 		= INIT_OPCODE(SBC, IMD_MO, 2, 2),
	[0xEA] 		= INIT_OPCODE(NOP, IMP_MO, 1, 2),
	[0xEB] 		= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0xEC] 		= INIT_OPCODE(CPX, ABS_MO, 3, 4),
	[0xED] 		= INIT_OPCODE(SBC, ABS_MO, 3, 4),
	[0xEE] 		= INIT_OPCODE(INC, ABS_MO, 3, 6),
	[0xEF] 		= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0xF0] 		= INIT_OPCODE(BEQ, REL_MO, 2, 2),
	[0xF1] 		= INIT_OPCODE(SBC, YIN_MO, 2, 5),
	[0xF2 ... 0xF4]	= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0xF5] 	        = INIT_OPCODE(SBC, ZPX_MO, 2, 4),
	[0xF6] 	        = INIT_OPCODE(INC, ZPX_MO, 2, 6),
	[0xF7] 	        = INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0xF8] 	        = INIT_OPCODE(SED, IMP_MO, 1, 2),
	[0xF9] 	        = INIT_OPCODE(SBC, ABY_MO, 3, 4),
	[0xFA ... 0xFC]	= INIT_OPCODE(XXX, IMP_MO, 1, 0),
	[0xFD] 		= INIT_OPCODE(SBC, ABX_MO, 3, 4),
	[0xFE] 		= INIT_OPCODE(INC, ABX_MO, 3, 7),
	[0xFF] 		= INIT_OPCODE(XXX, IMP_MO, 1, 0)
};

void load_prog (FILE *fp, word_t offset) {
	/* 
	 * 		RESUMO:
	 *
	 * Função de estimação do tamanho do arquivo
	 * e de carregamento de programa na memória
	 * virtual do emulador.
	 *
	 */

	int size;
	fseek(fp, 0L, SEEK_END);
	size = ftell(fp);
	fseek(fp, 0L, SEEK_SET);
	fread(PROC_MEM + offset, 1, size, fp);
}

void init_globals () {
	/*
	 * 		RESUMO:
	 *
	 * Função de inicialização de variáveis globais
	 * para funcionamento devido do emulador (ao carregar
	 * a ROM na memória virtual).
	 *
	 */

	cycles = 0;
	SP = STACK_BEGIN - STACK_END;
	PC = READ_MEM(RESET_VEC_LO) | (READ_MEM(RESET_VEC_HI) << 8);
}

void decode_exec (opcode_t *op) {
	/*
	 * 		RESUMO:
	 *
	 * Função responsável pelos ciclos de decodificação
	 * e execução das instruções da UCP (Unidade Central de
	 * Processamento). Além da atualização das variáveis
	 * globais e dos registradores de uso restrito.
	 *
	 */

	op->addr_mode(op);
	op->func();
}

void print_disas (opcode_t *op) {
	/*
	 * 		RESUMO:
	 *
	 * Função de impressão de características do
	 * código de máquina em questões de dissassembly.
	 *
	 */

	printf(
		"0x%04X: \t\t %s %s\n", 
		PC, 
		op->name, 
		op->args
	);
}

void debug_probe (opcode_t *op) {
	/*
	 * 		RESUMO:
	 *
	 * Função de debug dos prnicipais registradores 
	 * e var's internas da máquina de Turing.
	 *
	 */

	printf("****************************\n");
	printf("* REGS. DE USO ESPECÍFICO: *\n");
	printf("* X = 0x%.2X	Y = 0x%.2X   *\n", X, Y);
	printf("* PC = 0x%.4X 	len = %d    *\n", PC, op->num_bytes);
	printf("* SP = 0x%.4X 		   *\n", SP + STACK_END);
	printf("* SR = 0x%.2X 		   *\n", SR);
	printf("* AC = 0x%.4X 		   *\n", AC);
	printf("* ARGS = 0x%.4X		   *\n", *ARG);
	printf("* cycles = %d 		   *\n", cycles);
	printf("****************************\n");

	getchar();
}

void handle_sigint (int sig) {
	/*
	 * 		RESUMO:
	 *
	 * Função de manejo de sinal de interrupção
	 * (acaba com loop principal de emulação).
	 * É ativada a partir da combinação <Ctrl-c>
	 *
	 */

	if (sig == SIGINT)
		stop = 1;
}

int main (int argc, char *argv[]) {
	if (argc != 2) {
		print_error("Quantidade errada de argumentos (apenas o nome do programa)");
		return 1;
	}

	// Ciclo de carregamento de programa (input) na memória virtual.
	FILE *input = fopen(argv[1], "r");
	load_prog(input, LOAD_REG);
	fclose(input);

	/* INÍCIO DO CICLO DE DEBUG */
	WRITE_MEM(RESET_VEC_HI, ((LOAD_REG) >> 8) & 0xFF);
	WRITE_MEM(RESET_VEC_LO,  (LOAD_REG) 	  & 0xFF);
	/* FIM DO CILCO DE DEBUG    */

	// Inicialização de var. globais.
	init_globals();

	// Desenvolvimento do ciclo de execução do programa
	signal(SIGINT, handle_sigint);
	for (byte_t idx = READ_MEM(PC); stop == 0; idx = READ_MEM(PC)) {
		decode_exec(inst_mat + idx);
		print_disas(inst_mat + idx);
		debug_probe(inst_mat + idx);

		// Inserir aqui manejo de interrupção

		PC 	+= inst_mat[idx].num_bytes;
		cycles 	+= inst_mat[idx].cycles;

		if (STACK_FLAG) {
			print_error("Stack Overflow.");
			break;
		}
	}

	if (stop == 1)
		printf("Loop principal encerrado com sucesso.\n");

	return 0;
}
