#include <stdio.h>
#include <stdlib.h>

#ifndef  __6502_H__
#include "6502_proc.h"
#endif

#ifndef  __UI_H__
#include "ui_asm.h"
#endif

/* 	IMPLEMENTAÇAO DE TUI 
 *
 *  	Exemplo baseado na implementação de TUI
 *  	apresentada nos módulos de implementação 
 *  	da biblioteca PDCursesMod, usada como
 *  	base para este projeto. O arquivo modelo
 *  	pode ser encontrado no repositório do link:
 *
 *  	----------------------------------------
 *
 *  	https://github.com/Bill-Gray/PDCursesMod
 *
 *  	demos/tui.c & demos/tui.h 
 * 	
 * 	----------------------------------------
 *
 */

int nlines = 0;
int ncols  = 0;
int begx   = 0;
int begy   = 0;
WINDOW *main_win;
WINDOW *sub_wins[3];

void init_main_win (void) {
	/*
	 *  			RESUMO:
	 *
	 *  	Função responsável por criar janela (TUI) 
	 *  	e subjanelas em que estarão presentes o 
	 *  	desassembler, a pilha e a relação de
	 *  	registradores do emulador.
 	 */

	main_win = newwin(nlines, ncols, begx, begy);
	if (main_win == NULL) {
		print_error("Erro na criação da janela.");
		return;
	}

	sub_wins[DASM_SW] = subwin(main_win, NL_SW, NC_SW, BX_DASM, BY_DASM);
	sub_wins[STAC_SW] = subwin(main_win, NL_SW, NC_SW, BX_STAC, BY_STAC);
	sub_wins[REGS_SW] = subwin(main_win, NL_SW, NC_SW, BX_REGS, BY_REGS);
	for (int i = 0; i < 3; i++) {
		if (sub_wins[i] == NULL) {
			print_error("Erro na criação subjanelas.");
			return;
		}
	}

	wrefresh(main_win);
}

void destroy_main_win (void) {
	/*
	 *  			RESUMO:
	 *
	 *  	Função responsável por deletar janela (TUI) 
	 *  	em que estará presente o desassembler do
	 *  	emulador.
 	 */

	int err = 0; 
	for (int i = 0; i < 3; i++) {
		err += delwin(sub_wins[i]);
	}

	err += delwin(main_win);

	if (err != 0) {
		print_error("Erro na destruição da janela.");
	}
}

void print_dasm (char *str) {
	/*
	 *  			RESUMO:
	 *
	 *	Função de escrita na subjanela do dis-
	 *	assembler.
 	 */

	wprintw(sub_wins[DASM_SW], str);
	wrefresh(sub_wins[DASM_SW]);
	wrefresh(main_win);
}

void print_stac (char *str) {
	/*
	 *  			RESUMO:
	 *
	 *	Função de escrita na subjanela da pilha.
 	 */

	wprintw(sub_wins[STAC_SW], str);
	wrefresh(sub_wins[STAC_SW]);
	wrefresh(main_win);
}

void print_regs (char *str) {
	/*
	 *  			RESUMO:
	 *
	 *	Função de escrita na subjanela dos
	 *	registradores e memória.
 	 */

	wprintw(sub_wins[REGS_SW], str);
	wrefresh(sub_wins[REGS_SW]);
	wrefresh(main_win);
}

int key_input (void) {
	/*
	 *  			RESUMO:
	 *
	 *	Função responsável por captura de 
	 *	inputs do teclado sobre a janela
	 *	principal da TUI. Caso tecla ESC
	 *	seja apertada, destrói janela e 
	 *	subjanelas.
 	 */

	int c;
	keypad(main_win, TRUE);

	switch (c = wgetch(main_win)) {
		/* TODO: DESENVOLVER LÓGICA AQUI */
		case KEY_ESC:
			destroy_main_win();
			break;
	}

	return c;
}
