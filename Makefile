#################################################################
# 	ARQUIVO DE RECEITA PARA BUILD DE EMULADOR DE 6502	#
#################################################################

# DECLARAÇÃO DE VAIRÁVEIS DE SISTEMA.

# CC 	= gcc
CC 	= clang
TARGET	= main
NAME	= emu
LIB_PRC = 6502_proc
CFLAGS  = -g -Wall
INFLAGS = -c -Wall

# DECLARAÇÃO DE RECEITAS E MODOS DE PREPARO.

all: target $(LIB_PRC) $(TARGET)
	$(CC) $(CFLAGS) -o $(NAME) target/$(TARGET).o target/$(LIB_PRC).o

target:
	mkdir target

$(TARGET): src/$(LIB_PRC).h target
	$(CC) $(INFLAGS) src/$(TARGET).c -o target/$(TARGET).o

$(LIB_PRC): src/$(LIB_PRC).h target
	$(CC) $(INFLAGS) src/$(LIB_PRC).c -o target/$(LIB_PRC).o

clean: target
	rm target/*.o
